require 'rails_helper'

RSpec.describe "Visitiors API V1 as :admin", type: :request do
  let(:user) { create(:user) }
  let(:retail_chain) { create(:retail_chain) }
  
  context "GET /retail_chains/:retail_chain_id/visitors" do
    let(:url) { "/v1/retail_chains/#{retail_chain.id}/visitors" }
    let!(:visitors) { create_list(:visitor, 5, retail_chain: retail_chain) }

    context "without :name filter" do
      it "returns all visitors of a Retail Chain" do
        get url, headers: auth_header(user)
        expected_visitor = visitors.as_json(only: %i(id name))
        expected_visitor.map! { |visitor| visitor.merge('avatar_url' => '') }
        expect(body_json['visitors']).to eq expected_visitor
      end

      it "returns success status" do
        get url, headers: auth_header(user)
        expect(response).to have_http_status(:ok)
      end
    end

    context "with :name filter" do
      let!(:search_param) { { name: "Example" } }
      let!(:records_to_find) do 
        (0..3).to_a.map { |index| create(:visitor, name: "Example #{index}", retail_chain: retail_chain) }
      end

      it "returns visitors filtered by name" do
        get "#{url}?name=#{search_param[:name]}", headers: auth_header(user)
        expected_records = records_to_find.as_json(only: %i(id name))
        expected_records.map! { |record| record.merge!('avatar_url' => '')}
        expect(body_json['visitors']).to eq expected_records
      end

      it "does not returns visitors out of filter" do
        get "#{url}?name=#{search_param[:name]}", headers: auth_header(user)
        visitors_to_ignore = visitors.as_json(only: %i(id name))
        visitors_to_ignore.map! { |record| record.merge!('avatar_url' => '')}
        expect(body_json['visitors']).to_not include(*visitors_to_ignore)
      end

      it "does not returns visitors from other retail chain" do
        another_visitor = create(:visitor, name: "Example 4")
        get "#{url}?name=#{search_param[:name]}", headers: auth_header(user)
        visitor_to_ignore = another_visitor.as_json(only: %i(id name))
        visitor_to_ignore.merge!('avatar_url' => '')
        expect(body_json['visitors']).to_not include(visitor_to_ignore)
      end

      it "returns success status" do
        get "#{url}?name=#{search_param[:name]}", headers: auth_header(user)
        expect(response).to have_http_status(:ok)
      end
    end
  end

  context "POST /retail_chains/:retail_chain_id/visitors" do
    let(:url) { "/v1/retail_chains/#{retail_chain.id}/visitors" }
    
    context "with valid params" do
      let(:visitor_params) { { visitor: attributes_for(:visitor) }.to_json }

      it 'adds a new Visitor to Retail Chain' do
        expect do
          post url, headers: auth_header(user), params: visitor_params
        end.to change(retail_chain.visitors, :count).by(1)
      end

      it 'returns last added visitor' do
        post url, headers: auth_header(user), params: visitor_params
        expected_visitor = Visitor.last.as_json(only: %i(id name)).merge('avatar_url' => "")
        expect(body_json['visitor']).to eq expected_visitor
      end

      it 'returns success status' do
        post url, headers: auth_header(user), params: visitor_params
        expect(response).to have_http_status(:ok)
      end
    end
    
    context "with image" do
      let(:visitor_params) { { visitor: attributes_for(:visitor, :with_image) } }

      it 'adds a new Visitor to Retail Chain' do
        expect do
          post url, headers: auth_header(user), params: visitor_params
        end.to change(retail_chain.visitors, :count).by(1)
      end

      it 'adds a new ActiveStorage Blob chain' do
        expect do
          post url, headers: auth_header(user), params: visitor_params
        end.to change(ActiveStorage::Blob, :count).by(1)
      end

      it 'returns last added Visitor' do
        post url, headers: auth_header(user), params: visitor_params
        expected_visitor = Visitor.last.as_json(only: %i(id name))
        expected_visitor['avatar_url'] = rails_blob_url(Visitor.last.avatar)
        expect(body_json['visitor']).to eq expected_visitor
      end

      it 'returns success status' do
        post url, headers: auth_header(user), params: visitor_params
        expect(response).to have_http_status(:ok)
      end
    end

    context "with invalid params" do
      let(:visitor_invalid_params) do 
        { visitor: attributes_for(:visitor, name: nil) }.to_json
      end

      it 'does not add a new Visitor for Retail Chain' do
        expect do
          post url, headers: auth_header(user), params: visitor_invalid_params
        end.to_not change(retail_chain.visitors, :count)
      end

      it 'returns error message' do
        post url, headers: auth_header(user), params: visitor_invalid_params
        expect(body_json['errors']['fields']).to have_key('name')
      end

      it 'returns unprocessable_entity status' do
        post url, headers: auth_header(user), params: visitor_invalid_params
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  context "PATCH /visitors/:id" do
    let!(:visitor) { create(:visitor) }
    let(:url) { "/v1/visitors/#{visitor.id}" }

    context "with valid params" do
      let(:new_name) { "New name" }
      let(:visitor_params) { { visitor: { name: new_name } }.to_json }

      it 'updates Visitor' do
        patch url, headers: auth_header(user), params: visitor_params
        visitor.reload
        expect(visitor.name).to eq new_name 
      end

      it 'returns updated Visitor' do
        patch url, headers: auth_header(user), params: visitor_params
        visitor.reload
        expected_visitor = visitor.as_json(only: %i(id name)).merge('avatar_url' => "")
        expect(body_json['visitor']).to eq expected_visitor
      end

      it 'returns success status' do
        patch url, headers: auth_header(user), params: visitor_params
        expect(response).to have_http_status(:ok)
      end
    end

    context "with image" do
      let(:new_name) { "New name" }
      let(:avatar) { fixture_file_upload("spec/support/images/sample_avatar.jpg") }
      let(:visitor_params) { { visitor: { avatar: avatar } } }

      it 'adds a new ActiveStorage Blob chain' do
        expect do
          patch url, headers: auth_header(user), params: visitor_params
        end.to change(ActiveStorage::Blob, :count).by(1)
      end

      it 'returns updated Visitor' do
        patch url, headers: auth_header(user), params: visitor_params
        visitor.reload
        expected_visitor = visitor.as_json(only: %i(id name))
        expected_visitor['avatar_url'] = rails_blob_url(visitor.avatar)
        expect(body_json['visitor']).to eq expected_visitor
      end

      it 'returns success status' do
        patch url, headers: auth_header(user), params: visitor_params
        expect(response).to have_http_status(:ok)
      end
    end

    context "with invalid params" do
      let(:visitor_invalid_params) do 
        { visitor: attributes_for(:visitor, name: nil) }.to_json
      end

      it 'does not update visitor' do
        old_name = visitor.name
        patch url, headers: auth_header(user), params: visitor_invalid_params
        visitor.reload
        expect(visitor.name).to eq old_name
      end

      it 'returns error message' do
        patch url, headers: auth_header(user), params: visitor_invalid_params
        expect(body_json['errors']['fields']).to have_key('name')
      end

      it 'returns unprocessable_entity status' do
        patch url, headers: auth_header(user), params: visitor_invalid_params
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  context "DELETE /visitors/:id" do
    let!(:visitor) { create(:visitor, :with_image) }
    let(:url) { "/v1/visitors/#{visitor.id}" }

    it 'removes visitor' do
      expect do  
        delete url, headers: auth_header(user)
      end.to change(Visitor, :count).by(-1)
    end

    it 'removes avatar' do
      expect do  
        delete url, headers: auth_header(user)
      end.to change(ActiveStorage::Blob, :count).by(-1)
    end

    it 'returns success status' do
      delete url, headers: auth_header(user)
      expect(response).to have_http_status(:ok)
    end

    it 'does not return any body content' do
      delete url, headers: auth_header(user)
      expect(body_json).to_not be_present
    end
  end
end
