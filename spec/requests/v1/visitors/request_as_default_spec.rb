require 'rails_helper'

RSpec.describe "Retail chains API V1 as :default", type: :request do
  let(:user) { create(:user, :default_profile) }
  let(:retail_chain) { create(:retail_chain) }
  
  context "GET /retail_chains" do
    let(:url) { "/v1/retail_chains/#{retail_chain.id}/visitors" }
    let!(:visitors) { create_list(:visitor, 5, retail_chain: retail_chain) }
    
    before(:each) do
      get url, headers: auth_header(user)
    end

    include_examples "forbidden access"
  end

  context "POST /retail_chains" do
    let(:url) { "/v1/retail_chains/#{retail_chain.id}/visitors" }
    let(:visitor_params) { { visitor: attributes_for(:visitor) }.to_json }

    before(:each) do
      post url, headers: auth_header(user), params: visitor_params
    end

    include_examples "forbidden access"
  end

  context "PATCH /retail_chains/:id" do
    let!(:visitor) { create(:visitor) }
    let(:url) { "/v1/visitors/#{visitor.id}" }
    let(:new_name) { "New name" }
    let(:visitor_params) { { visitor: { name: new_name } }.to_json }

    before(:each) do
      patch url, headers: auth_header(user), params: visitor_params
    end

    include_examples "forbidden access"
  end

  context "DELETE /retail_chains/:id" do
    let!(:visitor) { create(:visitor) }
    let(:url) { "/v1/visitors/#{visitor.id}" }

    before(:each) do
      delete url, headers: auth_header(user)
    end
    
    include_examples "forbidden access"
  end
end
