require 'rails_helper'

RSpec.describe "Retail chains API V1 as unauthenticated user", type: :request do
  context "GET /retail_chains" do
    let!(:retail_chains) { create_list(:retail_chain, 5) }
    
    before(:each) do
      get '/v1/retail_chains'
    end

    include_examples "unauthenticated access"
  end

  context "POST /retail_chains" do
    let(:retail_chain_params) { { retail_chain: attributes_for(:retail_chain) }.to_json }

    before(:each) do
      post '/v1/retail_chains', params: retail_chain_params
    end

    include_examples "unauthenticated access"
  end

  context "PATCH /retail_chains/:id" do
    let!(:retail_chain) { create(:retail_chain) }
    let(:new_cnpj) { CNPJGenerator.unique }
    let(:retail_chain_params) { { retail_chain: { cnpj: new_cnpj } }.to_json }

    before(:each) do
      patch "/v1/retail_chains/#{retail_chain.id}", params: retail_chain_params
    end

    include_examples "unauthenticated access"
  end

  context "DELETE /retail_chains/:id" do
    let!(:retail_chain) { create(:retail_chain) }

    before(:each) do
      delete "/v1/retail_chains/#{retail_chain.id}"
    end
    
    include_examples "unauthenticated access"
  end
end
