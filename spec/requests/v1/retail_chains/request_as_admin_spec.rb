require 'rails_helper'

RSpec.describe "Retail chains API V1 as :admin", type: :request do
  let(:user) { create(:user) }
  
  context "GET /retail_chains" do
    let!(:retail_chains) { create_list(:retail_chain, 5) }

    context "without :name filter" do
      it "returns all retail chains" do
        get '/v1/retail_chains', headers: auth_header(user)
        expect(body_json['retail_chains']).to eq retail_chains.as_json(only: %i(id name cnpj))
      end

      it "returns success status" do
        get '/v1/retail_chains', headers: auth_header(user)
        expect(response).to have_http_status(:ok)
      end
    end

    context "with :name filter" do
      let!(:search_param) { { name: "Example" } }
      let!(:records_to_find) do 
        (0..3).to_a.map { |index| create(:retail_chain, name: "Example #{index}") }
      end

      it "returns retail chains filtered by name" do
        get "/v1/retail_chains?name=#{search_param[:name]}", headers: auth_header(user)
        expect(body_json['retail_chains']).to eq records_to_find.as_json(only: %i(id name cnpj))
      end

      it "does not return retail chains out of filter" do
        get "/v1/retail_chains?name=#{search_param[:name]}", headers: auth_header(user)
        retail_chains_to_ignore = retail_chains.as_json(only: %i(id name cnpj))
        expect(body_json['retail_chains']).to_not include(*retail_chains_to_ignore)
      end

      it "returns success status" do
        get "/v1/retail_chains?name=#{search_param[:name]}", headers: auth_header(user)
        expect(response).to have_http_status(:ok)
      end
    end
  end

  context "POST /retail_chains" do
    context "with valid params" do
      let(:retail_chain_params) { { retail_chain: attributes_for(:retail_chain) }.to_json }

      it 'adds a new retail chain' do
        expect do
          post '/v1/retail_chains', headers: auth_header(user), params: retail_chain_params
        end.to change(RetailChain, :count).by(1)
      end

      it 'returns last added retail chain' do
        post '/v1/retail_chains', headers: auth_header(user), params: retail_chain_params
        expected_retail_chain = RetailChain.last.as_json(only: %i(id name cnpj))
        expect(body_json['retail_chain']).to eq expected_retail_chain
      end

      it 'returns success status' do
        post '/v1/retail_chains', headers: auth_header(user), params: retail_chain_params
        expect(response).to have_http_status(:ok)
      end
    end

    context "with invalid params" do
      let(:retail_chain_invalid_params) do 
        { retail_chain: attributes_for(:retail_chain, cnpj: nil) }.to_json
      end

      it 'does not add a new retail chain' do
        expect do
          post '/v1/retail_chains', headers: auth_header(user), params: retail_chain_invalid_params
        end.to_not change(RetailChain, :count)
      end

      it 'returns error message' do
        post '/v1/retail_chains', headers: auth_header(user), params: retail_chain_invalid_params
        expect(body_json['errors']['fields']).to have_key('cnpj')
      end

      it 'returns unprocessable_entity status' do
        post '/v1/retail_chains', headers: auth_header(user), params: retail_chain_invalid_params
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  context "PATCH /retail_chains/:id" do
    let!(:retail_chain) { create(:retail_chain) }

    context "with valid params" do
      let(:new_cnpj) { CNPJGenerator.unique }
      let(:retail_chain_params) { { retail_chain: { cnpj: new_cnpj } }.to_json }

      it 'updates retail chain' do
        patch "/v1/retail_chains/#{retail_chain.id}", headers: auth_header(user), params: retail_chain_params
        retail_chain.reload
        expect(retail_chain.cnpj).to eq new_cnpj
      end

      it 'returns updated retail chain' do
        patch "/v1/retail_chains/#{retail_chain.id}", headers: auth_header(user), params: retail_chain_params
        retail_chain.reload
        expected_retail_chain = retail_chain.as_json(only: %i(id name cnpj))
        expect(body_json['retail_chain']).to eq expected_retail_chain
      end

      it 'returns success status' do
        patch "/v1/retail_chains/#{retail_chain.id}", headers: auth_header(user), params: retail_chain_params
        expect(response).to have_http_status(:ok)
      end
    end

    context "with invalid params" do
      let(:retail_chain_invalid_params) do 
        { retail_chain: attributes_for(:retail_chain, cnpj: nil) }.to_json
      end

      it 'does not update retail chain' do
        old_cnpj = retail_chain.cnpj
        patch "/v1/retail_chains/#{retail_chain.id}", headers: auth_header(user), 
                                                      params: retail_chain_invalid_params
        retail_chain.reload
        expect(retail_chain.cnpj).to eq old_cnpj
      end

      it 'returns error message' do
        patch "/v1/retail_chains/#{retail_chain.id}", headers: auth_header(user), 
                                                      params: retail_chain_invalid_params
        expect(body_json['errors']['fields']).to have_key('cnpj')
      end

      it 'returns unprocessable_entity status' do
        patch "/v1/retail_chains/#{retail_chain.id}", headers: auth_header(user), 
                                                      params: retail_chain_invalid_params
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  context "DELETE /retail_chains/:id" do
    let!(:retail_chain) { create(:retail_chain) }

    it 'removes retail chain' do
      expect do  
        delete "/v1/retail_chains/#{retail_chain.id}", headers: auth_header(user)
      end.to change(RetailChain, :count).by(-1)
    end

    it 'returns success status' do
      delete "/v1/retail_chains/#{retail_chain.id}", headers: auth_header(user)
      expect(response).to have_http_status(:ok)
    end

    it 'does not return any body content' do
      delete "/v1/retail_chains/#{retail_chain.id}", headers: auth_header(user)
      expect(body_json).to_not be_present
    end

    it 'sets associated users with empty id' do
      retail_chain_user = create(:user, retail_chain: retail_chain)
      delete "/v1/retail_chains/#{retail_chain.id}", headers: auth_header(user)
      retail_chain_user.reload
      expect(retail_chain_user.retail_chain).to_not be_present
    end

    it 'removes all associated visitors' do
      visitors = create_list(:visitor, 3, retail_chain: retail_chain)
      delete "/v1/retail_chains/#{retail_chain.id}", headers: auth_header(user)
      reloaded_visitors = Visitor.where(id: visitors.map(&:id))
      expect(reloaded_visitors.count).to eq 0
    end

    it 'does not remove unassociated visitors' do
      visitors = create_list(:visitor, 3)
      delete "/v1/retail_chains/#{retail_chain.id}", headers: auth_header(user)
      reloaded_visitors = Visitor.where(id: visitors.map(&:id))
      expect(reloaded_visitors.count).to eq visitors.count
    end
  end
end
