require "rails_helper"

describe "User sign out", type: :request do
  let(:user) { create(:user) }

  it "cleans user token" do
    delete '/v1/sign_out', headers: auth_header(user)
    user.reload
    expect(user.token).to_not be_present
  end

  it "returns status 200" do
    delete '/v1/sign_out', headers: auth_header(user)
    expect(response.status).to eq 200
  end

  it "doesn't return any body content" do
    delete '/v1/sign_out', headers: auth_header(user)
    expect(body_json).to_not be_present
  end
end