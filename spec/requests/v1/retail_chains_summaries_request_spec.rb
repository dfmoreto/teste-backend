require 'rails_helper'

RSpec.describe "V1::RetailChainsSummaries", type: :request do
  context "GET /v1/retail_chains/summaries" do
    let!(:first_retail_chain) { create(:retail_chain) }
    let!(:second_retail_chain) { create(:retail_chain) }
    let!(:first_visitor) { create(:visitor, retail_chain: first_retail_chain) }
    let!(:second_visitor) { create(:visitor, retail_chain: first_retail_chain) }

    it "returns all retail chains" do
      get '/v1/retail_chains/summaries', headers: { 'Accept' => 'application/json' }
      first_retail_chain_summary = first_retail_chain.as_json(only: %i(name cnpj))
      first_retail_chain_summary['visitors'] = [first_visitor.name, second_visitor.name].sort
      second_retail_chain_summary = second_retail_chain.as_json(only: %i(name cnpj))
      second_retail_chain_summary['visitors'] = []
      expected_response = { 
        'total' => 2, 'retail_chain_visitors' => [first_retail_chain_summary, second_retail_chain_summary]
      }
      expect(body_json).to eq expected_response
    end

    it "returns success status" do
      get '/v1/retail_chains/summaries', headers: { 'Accept' => 'application/json' }
      expect(response).to have_http_status(:ok)
    end
  end
end
