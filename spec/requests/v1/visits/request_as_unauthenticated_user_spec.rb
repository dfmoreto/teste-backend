require 'rails_helper'

RSpec.describe "Visits API V1 as unauthenticated user", type: :request do
  let!(:visitor) { create(:visitor) }

  context "POST /retail_chains" do
    let(:url) { "/v1/visitors/#{visitor.id}/visits" }
    let(:visit_params) { { visit: attributes_for(:visit) }.to_json }

    before(:each) do
      post url, params: visit_params
    end

    include_examples "unauthenticated access"
  end

  context "PATCH /retail_chains/:id" do
    let!(:visit) { create(:visit, visitor: visitor) }
    let(:url) { "/v1/visitors/#{visitor.id}/visits/#{visit.id}" }
    let(:new_entry_date) { Time.zone.now.strftime("%Y-%m-%d %H:%M:%S") }
    let(:visit_params) { { visit: { entry_date: new_entry_date } }.to_json }

    before(:each) do
      patch url, params: visit_params
    end

    include_examples "unauthenticated access"
  end
end
