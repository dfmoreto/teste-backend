require 'rails_helper'

RSpec.describe "Visits API V1 as :default", type: :request do
  let!(:visitor) { create(:visitor) }

  context "when user belongs to same Retail Chain" do
    let(:user) { create(:user, :default_profile, retail_chain: visitor.retail_chain) }

    context "POST /visitors/:visitor_id/visits" do
      let(:url) { "/v1/visitors/#{visitor.id}/visits" }

      context "with valid params" do
        let(:visit_params) { { visit: attributes_for(:visit) }.to_json }

        it 'adds a new Visit' do
          expect do
            post url, headers: auth_header(user), params: visit_params
          end.to change(Visit, :count).by(1)
        end

        it 'returns last added Visit' do
          post url, headers: auth_header(user), params: visit_params
          expected_visit = Visit.last.as_json(only: %i(id entry_date exit_date))
          expect(body_json['visit']).to eq expected_visit
        end

        it 'returns success status' do
          post url, headers: auth_header(user), params: visit_params
          expect(response).to have_http_status(:ok)
        end
      end

      context "with invalid params" do
        let(:visit_invalid_params) do 
          { visit: attributes_for(:visit, entry_date: nil) }.to_json
        end

        it 'does not add a new Visit' do
          expect do
            post url, headers: auth_header(user), params: visit_invalid_params
          end.to_not change(Visit, :count)
        end

        it 'returns error message' do
          post url, headers: auth_header(user), params: visit_invalid_params
          expect(body_json['errors']['fields']).to have_key('entry_date')
        end

        it 'returns unprocessable_entity status' do
          post url, headers: auth_header(user), params: visit_invalid_params
          expect(response).to have_http_status(:unprocessable_entity)
        end
      end
    end

    context "PATCH /visitors/:visitor_id/visits/:id" do
      let!(:visit) { create(:visit, visitor: visitor) }
      let(:url) { "/v1/visitors/#{visitor.id}/visits/#{visit.id}" }

      context "with valid params" do
        let(:new_entry_date) { Time.zone.now.strftime("%Y-%m-%d %H:%M:%S") }
        let(:visit_params) { { visit: { entry_date: new_entry_date } }.to_json }

        it 'updates Visit' do
          patch url, headers: auth_header(user), params: visit_params
          visit.reload
          updated_entry_date = visit.entry_date.strftime("%Y-%m-%d %H:%M:%S")
          expect(updated_entry_date).to eq new_entry_date
        end

        it 'returns updated Visit' do
          patch url, headers: auth_header(user), params: visit_params
          visit.reload
          expected_visit = visit.as_json(only: %i(id entry_date exit_date))
          expect(body_json['visit']).to eq expected_visit
        end

        it 'returns success status' do
          patch url, headers: auth_header(user), params: visit_params
          expect(response).to have_http_status(:ok)
        end
      end

      context "with invalid params" do
        let(:visit_invalid_params) do 
          { visit: attributes_for(:visit, entry_date: nil) }.to_json
        end

        it 'does not update Visit' do
          old_entry_date = visit.entry_date.strftime("%Y-%m-%d %H:%M:%S")
          patch url, headers: auth_header(user), params: visit_invalid_params
          visit.reload
          new_entry_date = visit.entry_date.strftime("%Y-%m-%d %H:%M:%S") 
          expect(new_entry_date).to eq old_entry_date
        end

        it 'returns error message' do
          patch url, headers: auth_header(user), params: visit_invalid_params
          expect(body_json['errors']['fields']).to have_key('entry_date')
        end

        it 'returns unprocessable_entity status' do
          patch url, headers: auth_header(user), params: visit_invalid_params
          expect(response).to have_http_status(:unprocessable_entity)
        end
      end
    end
  end

  context "when user does not belong to same Retail Chain" do
    let(:user) { create(:user, :default_profile) }

    context "POST /visitors/:visitor_id/visits" do
      let(:url) { "/v1/visitors/#{visitor.id}/visits" }
      let(:visit_params) { { visit: attributes_for(:visit) }.to_json }

      before(:each) do
        post url, headers: auth_header(user), params: visit_params
      end

      include_examples "forbidden access"
    end

    context "PATCH /visitors/:visitor_id/visits/:id" do
      let!(:visit) { create(:visit, visitor: visitor) }
      let(:url) { "/v1/visitors/#{visitor.id}/visits/#{visit.id}" }
      let(:new_entry_date) { Time.zone.now.strftime("%Y-%m-%d %H:%M:%S") }
      let(:visit_params) { { visit: { entry_date: new_entry_date } }.to_json }

      before(:each) do
        patch url, headers: auth_header(user), params: visit_params
      end

      include_examples "forbidden access"
    end
  end
end
