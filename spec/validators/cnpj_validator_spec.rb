require "rails_helper"

class Validatable
  include ActiveModel::Validations
  attr_accessor :cnpj
  validates :cnpj, cnpj: true
end

describe CnpjValidator, type: :model do
  subject { Validatable.new }

  context "when it's out of CNPJ format" do
    before { subject.cnpj = '11.111.11/000-01' }

    it "should be invalid" do

      expect(subject.valid?).to be_falsey
    end

    it "adds an error on model" do
      subject.valid?
      expect(subject.errors.keys).to include(:cnpj)
    end
  end

  context "when number verification doesn't fit" do
    before { subject.cnpj = '11.111.111/0001-11' }

    it "should be invalid" do
      expect(subject.valid?).to be_falsey
    end

    it "adds an error on model" do
      subject.valid?
      expect(subject.errors.keys).to include(:cnpj)
    end
  end

  context "when cnpj has right format" do
    before { subject.cnpj = CNPJGenerator.random }

    it "should be valid" do
      expect(subject.valid?).to be_truthy
    end
  end
end