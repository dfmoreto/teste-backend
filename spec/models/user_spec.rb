require 'rails_helper'

RSpec.describe User, type: :model do
  it { is_expected.to have_secure_password }

  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:email) }
  it { is_expected.to validate_uniqueness_of(:email).case_insensitive }
  it { is_expected.to validate_uniqueness_of(:token) }
  it { is_expected.to validate_presence_of(:profile) }
  it { is_expected.to define_enum_for(:profile).with_values({ admin: 0, default: 1 }) }

  it { is_expected.to belong_to(:retail_chain).required(false) }

  it ":retail_chain should be present when user has :default profile" do
    subject.profile = :admin
    is_expected.to_not validate_presence_of(:retail_chain_id)
  end

  it ":retail_chain should be present when user has :default profile" do
    subject.profile = :default
    is_expected.to validate_presence_of(:retail_chain_id)
  end

  context "#from_token_payload" do
    let(:user) { create(:user, :with_token) }

    it "returns User by :token" do
      user_found = described_class.from_token_payload({ 'token' => user.token })
      expect(user).to eq user_found
    end

    it "can't find a User if token is invalid" do
      user_found = described_class.from_token_payload({ 'token' => 'some-random-token' })
      expect(user_found).to_not be_present
    end

    it "can't find a user with empty token" do
      user_token = user.token
      user.update(token: nil)
      user_found = described_class.from_token_payload({ 'token' => user_token })
      expect(user_found).to_not be_present
    end
  end

  it "#to_token_payload returns user id, email and token as payload" do
    subject = create(:user, :with_token)
    expected_payload = { sub: subject.id, email: subject.email, token: subject.token }
    expect(subject.to_token_payload).to eq(expected_payload)
  end

  context "#generate_token" do
    subject { create(:user) }

    it "fills a token" do
      subject.generate_token
      expect(subject.token).to be_present
    end

    it "doesn't generates a new token if it's already fulfilled" do
      subject.generate_token
      previous_token = subject.token
      subject.generate_token
      expect(subject.token).to eq previous_token
    end
  end
end
