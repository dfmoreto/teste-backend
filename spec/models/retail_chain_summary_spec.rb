require 'rails_helper'

RSpec.describe RetailChainSummary, type: :model do
  let!(:first_retail_chain) { create(:retail_chain) }
  let!(:second_retail_chain) { create(:retail_chain) }
  let!(:first_visitor) { create(:visitor, retail_chain: first_retail_chain) }
  let!(:second_visitor) { create(:visitor, retail_chain: first_retail_chain) }

  context "#call" do
    it "returns total of retail chains" do
      summary = described_class.call
      expect(summary[:total]).to eq 2
    end

    it "returns :name, :cnpf and visitors name of each retail chain" do
      summary = described_class.call
      first_retail_chain_summary = first_retail_chain.as_json(only: %i(name cnpj)).deep_symbolize_keys
      first_retail_chain_summary[:visitors] = [first_visitor.name, second_visitor.name].sort
      second_retail_chain_summary = second_retail_chain.as_json(only: %i(name cnpj)).deep_symbolize_keys
      second_retail_chain_summary[:visitors] = []
      expect(summary[:retail_chain_visitors]).to eq([first_retail_chain_summary, second_retail_chain_summary])
    end
  end
end
