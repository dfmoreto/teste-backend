require 'rails_helper'

RSpec.describe RetailChain, type: :model do
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:cnpj) }
  it { is_expected.to validate_uniqueness_of(:cnpj).case_insensitive }

  it { is_expected.to have_many(:users).dependent(:nullify) }
  it { is_expected.to have_many(:visitors).dependent(:destroy) }

  context "with invalid CNPJ" do
    it "sets error message when it's out of format" do
      retail_chain = build(:retail_chain, cnpj: '11.111111111')
      expect(retail_chain.valid?).to be_falsey
    end

    it "sets error message when it's not a valid record" do
      retail_chain = build(:retail_chain, cnpj: '11.111.111/0001-01')
      expect(retail_chain.valid?).to be_falsey
    end
  end

  it "when CNPJ is valid it's a valid record" do
    retail_chain = build(:retail_chain)
    expect(retail_chain.valid?).to be_truthy
  end

  it_behaves_like "name searchable concern", :retail_chain
end
