require 'rails_helper'

RSpec.describe Visit, type: :model do
  it { is_expected.to validate_presence_of :entry_date }
  it { is_expected.to belong_to :visitor }

  it ":exit_date should be greater and :entry_date" do
    subject.entry_date = Time.zone.now
    subject.exit_date = subject.entry_date - 1.hour
    subject.valid?
    expect(subject.errors).to have_key(:exit_date)
  end
end
