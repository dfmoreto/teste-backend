require 'rails_helper'

RSpec.describe TokenGenerator do
  context "#call" do
    it "generates a 20 length token" do
      token = described_class.call
      expect(token.length).to eq 20
    end

    it "generates a different token for another user" do
      first_token = "some-random-token"
      second_token = "new-random-token"
      allow(SecureRandom).to receive(:hex).and_return(first_token, second_token)
      user = create(:user, token: first_token)
      expect(described_class.call).to eq second_token
    end
  end
end
