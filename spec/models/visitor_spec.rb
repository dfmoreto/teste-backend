require 'rails_helper'

RSpec.describe Visitor, type: :model do
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_uniqueness_of(:name).case_insensitive }

  it { is_expected.to belong_to :retail_chain }
  it { is_expected.to have_many :visits }

  it "has :avatar to perform upload" do
    avatar = fixture_file_upload Rails.root.join("spec/support/images/sample_avatar.jpg")
    expect do
      subject.avatar.attach(avatar)
    end.to_not raise_error
  end

  it_behaves_like "name searchable concern", :visitor
end
