module ApiHelper
  def auth_header(user)
    user.update(token: 'some-token')
    token = Knock::AuthToken.new(payload: { sub: user.id, email: user.email, token: user.token }).token
    { 'Authorization' => "Bearer #{token}", 'Content-Type' => 'application/json', 
      'Accept' => 'application/json' }
  end
  
  def body_json(symbolize_keys: false)
    json = JSON.parse(response.body)
    symbolize_keys ? json.deep_symbolize_keys : json
  rescue
    return {} 
  end
end

RSpec.configure do |config|
  config.include ApiHelper
end