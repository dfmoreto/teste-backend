class CNPJGenerator
  VALID_CNPJS = %W(
    74.397.186/0001-09 36.295.624/0001-20 51.242.708/0001-90 62.365.301/0001-76 30.609.395/0001-95
    31.067.575/0001-55 50.732.849/0001-28 65.372.203/0001-54 50.617.909/0001-61 31.151.786/0001-71
    11.333.502/0001-50 32.246.947/0001-73 48.021.391/0001-57 90.946.623/0001-44 46.187.630/0001-18
    38.949.773/0001-91 32.567.451/0001-00 49.030.879/0001-03 53.534.870/0001-07 22.087.279/0001-25
  )

  def self.random
    VALID_CNPJS.sample
  end

  def self.unique
    @cnpjs ||= []
    cnpj = random
    cnpj = random while @cnpjs.include?(cnpj)
    @cnpjs << cnpj
    cnpj
  end

  def self.clean
    @cnpjs = []
  end
end

RSpec.configure do |config|
  config.before(:each) { CNPJGenerator.clean }
end