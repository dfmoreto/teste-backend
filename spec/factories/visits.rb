FactoryBot.define do
  factory :visit do
    entry_date { (0..4).to_a.sample.days.ago }
    exit_date { 5.days.from_now }
    visitor
  end
end
