FactoryBot.define do
  factory :user do
    name { Faker::Name.name }
    email { Faker::Internet.email }
    password { "123456" }
    password_confirmation { "123456" }
    profile { :admin }
    token { nil }

    trait :with_token do
      after :create do |user|
        user.generate_token
      end
    end

    trait :default_profile do
      profile { :default }
      retail_chain
    end
  end
end
