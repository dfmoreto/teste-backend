FactoryBot.define do
  factory :visitor do
    name { Faker::Name.name }
    retail_chain

    trait :with_image do
      avatar { Rack::Test::UploadedFile.new(Rails.root.join("spec/support/images/sample_avatar.jpg")) }
    end
  end
end
