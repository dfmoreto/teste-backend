FactoryBot.define do
  factory :retail_chain do
    name { Faker::Company.name }
    cnpj { CNPJGenerator.unique }
  end
end
