class CreateVisits < ActiveRecord::Migration[6.0]
  def change
    create_table :visits do |t|
      t.datetime :entry_date
      t.datetime :exit_date
      t.references :visitor, null: false, foreign_key: true

      t.timestamps
    end
  end
end
