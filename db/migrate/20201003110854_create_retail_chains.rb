class CreateRetailChains < ActiveRecord::Migration[6.0]
  def change
    create_table :retail_chains do |t|
      t.string :name
      t.string :cnpj

      t.timestamps
    end
  end
end
