first_retail = RetailChain.create(name: "Retail Chain 01", cnpj: "45.697.133/0001-05")
second_retail = RetailChain.create(name: "Retail Chain 02", cnpj: "89.211.581/0001-14")

User.create(email: "admin@test.com", name: "Administrator", password: "123456", password_confirmation: "123456",
            profile: :admin)

User.create(email: "default01@test.com", name: "Defaul 01", password: "123456", password_confirmation: "123456",
              profile: :default, retail_chain: RetailChain.first)

User.create(email: "default02@test.com", name: "Default 02", password: "123456", password_confirmation: "123456",
                profile: :default, retail_chain: RetailChain.second)