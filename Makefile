APP := web
RUN := docker-compose run --rm $(APP)

build:
	mkdir -p tmp/db
	sudo chmod 775 -R tmp/db
	docker-compose build

start:
	docker-compose up

stop:
	docker-compose stop

clean: stop
	rm -f tmp/pids/server.pid
	sudo rm -rf tmp/db

seed:
	$(RUN) bin/run_migrations
	$(RUN) rails db:seed

rspec:
	$(RUN) rspec $(filter-out $@,$(MAKECMDGOALS))