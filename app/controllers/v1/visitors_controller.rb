module V1  
  class VisitorsController < ApiController
    before_action :retrict_access_to_admin

    def index
      @visitors = Visitor.where(retail_chain_id: params[:retail_chain_id]).search_by_name(params)
    end

    def create
      build_visitor
      save_visitor
    end

    def update
      load_visitor
      build_visitor
      save_visitor
    end

    def destroy
      load_visitor
      destroy_visitor
    end

    private

    def build_visitor
      @visitor ||= Visitor.new(retail_chain_id: params[:retail_chain_id])
      @visitor.attributes = visitor_params
    end

    def save_visitor
      if @visitor.save
        render :show
      else
        render_error(fields: @visitor.errors.messages)
      end
    end

    def load_visitor
      @visitor = Visitor.find(params[:id])
    end

    def destroy_visitor
      @visitor.destroy!
      @visitor.avatar.purge
      head :ok
    rescue
      render_error(fields: @visitor.errors.messages)
    end

    def visitor_params
      return {} unless params.has_key?(:visitor)
      params.require(:visitor).permit(:name, :avatar, :retail_chain_id)
    end
  end
end
