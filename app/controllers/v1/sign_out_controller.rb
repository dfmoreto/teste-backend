module V1
  class SignOutController < ApiController
    def destroy
      current_user.update!(token: nil)
      head :ok
    end
  end
end