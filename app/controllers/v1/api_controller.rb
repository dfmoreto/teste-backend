module V1
  class ApiController < ApplicationController
    class ForbiddenAccess < StandardError; end

    include Knock::Authenticable

    before_action :authenticate_user

    rescue_from ForbiddenAccess do 
      render_error(message: "Forbidden access", status: :forbidden)
    end

    def render_error(message: nil, fields: nil, status: :unprocessable_entity)
      render partial: 'v1/shared/error', locals: { message: message, fields: fields }, status: status
    end

    private

    def retrict_access_to_admin
      raise ForbiddenAccess unless current_user.admin?
    end
  end
end