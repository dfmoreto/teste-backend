module V1  
  class RetailChainsController < ApiController
    before_action :retrict_access_to_admin

    def index
      @retail_chains = RetailChain.all.search_by_name(params)
    end

    def create
      build_retail_chain
      save_retail_chain
    end

    def update
      load_retail_chain
      build_retail_chain
      save_retail_chain
    end

    def destroy
      load_retail_chain
      destroy_retail_chain
    end

    private

    def build_retail_chain
      @retail_chain ||= RetailChain.new(retail_chain_params)
      @retail_chain.attributes = retail_chain_params
    end

    def save_retail_chain
      if @retail_chain.save
        render :show
      else
        render_error(fields: @retail_chain.errors.messages)
      end
    end

    def load_retail_chain
      @retail_chain = RetailChain.find(params[:id])
    end

    def destroy_retail_chain
      if @retail_chain.destroy
        head :ok
      else
        render_error(fields: @retail_chain.errors.messages)
      end
    end

    def retail_chain_params
      return {} unless params.has_key?(:retail_chain)
      params.require(:retail_chain).permit(:name, :cnpj)
    end
  end
end
