module V1
  class RetailChainsSummariesController < ApiController
    skip_before_action :authenticate_user

    def index
      @summary = RetailChainSummary.call
    end
  end
end
