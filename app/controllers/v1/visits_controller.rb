module V1  
  class VisitsController < ApiController
    before_action :load_visitor
    before_action :retrict_default_user_access

    def create
      build_visit
      save_visit
    end

    def update
      load_visit
      build_visit
      save_visit
    end

    private

    def load_visitor
      @visitor = Visitor.find(params[:visitor_id])
    end

    def retrict_default_user_access
      return if current_user.admin?
      return if current_user.default? && current_user.retail_chain == @visitor.retail_chain
      raise ForbiddenAccess
    end

    def build_visit
      @visit ||= @visitor.visits.build
      @visit.attributes = visit_params
    end

    def save_visit
      if @visit.save
        render :show
      else
        render_error(fields: @visit.errors.messages)
      end
    end

    def load_visit
      @visit = @visitor.visits.find(params[:id])
    end

    def visit_params
      return {} unless params.has_key?(:visit)
      params.require(:visit).permit(:entry_date, :exit_date)
    end
  end
end