class RetailChainSummary
  def self.call
    retail_chain_visitors = RetailChain.all.map do |retail|
      visitors = retail.visitors.order(:name).pluck(:name)
      { name: retail.name, cnpj: retail.cnpj, visitors: visitors }
    end
    { total: RetailChain.count, retail_chain_visitors: retail_chain_visitors }
  end
end