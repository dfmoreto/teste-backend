class Visitor < ApplicationRecord
  include NameSearchable
  
  validates :name, presence: true, uniqueness: { case_sensitive: false }
  
  belongs_to :retail_chain
  has_many :visits
  
  has_one_attached :avatar
end
