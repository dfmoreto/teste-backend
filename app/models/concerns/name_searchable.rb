module NameSearchable
  extend ActiveSupport::Concern

  included do
    scope :search_by_name, -> (params) do
      return unless params.has_key?(:name)
      where("name LIKE ?", "%#{params[:name]}%")
    end
  end
end