class TokenGenerator
  TOKEN_LENGTH = 20

  def self.call
    token = generate_hex
    while User.find_by(token: token)
      token = generate_hex
    end
    token
  end

  private

  def self.generate_hex
    hex_length = TOKEN_LENGTH / 2
    SecureRandom.hex(hex_length)
  end
end