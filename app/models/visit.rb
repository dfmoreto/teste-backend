class Visit < ApplicationRecord
  validates :entry_date, presence: true
  validate :exit_date_after_than_entry_date

  belongs_to :visitor

  private

  def exit_date_after_than_entry_date
    if entry_date.present? && entry_date >= exit_date
      errors[:exit_date] << "can't be a date before :entry_date"
    end
  end
end
