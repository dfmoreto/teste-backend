class RetailChain < ApplicationRecord
  include NameSearchable
  
  validates :name, presence: true
  validates :cnpj, presence: true, uniqueness: { case_sensitive: false }, cnpj: true

  has_many :users, dependent: :nullify
  has_many :visitors, dependent: :destroy
end
