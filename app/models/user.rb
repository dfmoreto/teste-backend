class User < ApplicationRecord
  has_secure_password

  validates :name, presence: true
  validates :email, presence: true, uniqueness: { case_sensitive: false }
  validates :token, uniqueness: true, if: -> { token.present? }
  validates :profile, presence: true
  validates :retail_chain_id, presence: true, if: -> { default? }
  
  enum profile: { admin: 0, default: 1 }

  belongs_to :retail_chain, optional: true

  def self.from_token_payload(payload)
    self.find_by(token: payload['token'])
  end

  def generate_token
    return if self.token.present?
    self.token = TokenGenerator.call
    self.save!
  end

  def to_token_payload
    generate_token
    { sub: self.id, email: self.email, token: self.token }
  end
end
