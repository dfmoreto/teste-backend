json.visitor do
  json.id @visitor.id
  json.name @visitor.name
  json.avatar_url @visitor.avatar.present? ? rails_blob_url(@visitor.avatar) : ""
end