Rails.application.routes.draw do
  namespace :v1, constraints: { format: 'json' } do
    post 'sign_in' => 'user_token#create'
    delete 'sign_out' => 'sign_out#destroy'

    resources :retail_chains, shallow: true do
      resources :visitors
      get :summaries, to: "retail_chains_summaries#index", on: :collection
    end

    resources :visitors, only: [] do
      resources :visits, only: [:create, :update]
    end
  end
end
