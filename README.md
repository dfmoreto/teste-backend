# Teste Backend - Mob2Con

This is a code for a position on company Mob2Con. So, let me explain a little bit about what I've done =)

This is an API for CRUDing Retail Stores (or Chain as I name them on application) and record their visitors along with when they come in and out of a store.


## How to use it?

Basically we have some endpoints to be used by users with specifically permissions.

We have two profiles on app: `admin` and `default`.

**Admin** is the person responsible to manage everything. He can CRUD a *Retail Chain*, *Visitor* and record *Visit*.
**Default** is the profile permitted to record *Visit*.

There is also a **Retail Chain Summary** allowed to be accessed by everyone. Even for those who are not Authenticated.

As a feature to **Sign Up** a user was not requested, on seed file there are two users that will be created:
- **admin@test.com** with _Admin_ profile
- **default01@test.com** with _Default_ profile
- **default02@test.com** with _Default_ profile

As _Default_ profile User requires a **Retail Chain** associated (yeap, we can't have a _Default_ user without a **Retail Chain**), this seed will also create two Retail Chains.
- **Retail Chain 01** for user _default01@test.com_
- **Retail Chain 02** for user _default02@test.com_


## Building application

**!!!A LITTLE WARNING:** To avoid any misundertanding about it, the creadentials master key was added on `docker-compose.yml` as `RAILS_MASTER_KEY` env variable to make it easier to run application without any additional intervention. I wouldn't do it in a real environment. =)

Ok, so about the building....

We have two ways to do it, with Docker and without it. I'll explain both here.

### Docker way

There are some Make commands in this project to make this path easier. So, to go through this one, it **required** to have Docker and Docker compose installed.

1. Build application
    ```
    make build
    ```

2. Run seeds (it's not required, but I recommend you to avoid entering in console add new User and another stuffs)
    ```
    make seed
    ```

3. Start application
   ```
   make start
   ```

If you want to stop application you only need to do a `Ctrl+C` and wait containers to stop

And if you want to run tests:
```
make rspec
```

There is also a command to clean everything and start application from the scratch: 
```
make clean
```


### I don't want Docker!

Ok, so first you must have:

1. Postgres installed

2. Ruby 2.7.2 installed

3. This ENV variables set:
    - DB_USER for database user
    - DB_PASSWORD for database password
    - DB_HOST for database host

As soon as you have everything done:

1. Bundle application
```
bundle install
```

2. Create databases
```
rails db:create
```

3. Run migrations
```
rails db:migrate
```

4. Start the server
```
rails s
```

If you want to run tests: 
```
bundle exec rspec
```

Or seeds: 
```
rails db:seed
```

## And how can I use API?

Well, I have here two files if you want to import on Postman or Insomnia

[This one is for Insomnia](https://drive.google.com/file/d/1Kru02PqVyPD9iqSAi44T9JgfORQg3-4x/view?usp=sharing)

[And this one for Postman](https://drive.google.com/file/d/1gZjuNVEXwj7T0_J2m8_TfhA5tjyXJ-bj/view?usp=sharing)


There is also a list of Endpoints I've made in a Google Docs

[And here is the Google Docs](https://docs.google.com/document/d/1Bkq1pWaBY9ixyNGh7AwUeZqWVMx2JEmar6YUWsFX2sU/edit?usp=sharing)

But if you don't have any of these tools, here are tables listing the Endpoints

### Login

| Endpoint | What does it do? |
| ---------| -----------------|
| POST /v1/sign_in | Login on application |
| DELETE /v1/sign_out | Logout of application |

### Retail Chains
| Endpoint | What does it do? |
| ---------| -----------------|
| GET /v1/retail_chains | List of all Retail Chains |
| POST /v1/retail_chains | Add a new Retail Chain |
| PATCH /v1/retail_chains/:id | Update a Retail Chain |
| DELETE /v1/retail_chains/:id | Remove a Retail Chain |

### Visitors
| Endpoint | What does it do? |
| ---------| -----------------|
| GET /v1/retail_chains/:retail_chain_id/visitors | List of Visitors of a Retail Chain |
| POST /v1/retail_chains/:retail_chain_id/visitors | Add a new Retail Chain Visitor |
| PATCH /v1/visitors/:id | Update da Visitor |
| DELETE /v1/visitors/:id | Remove a Visitor |

### Visits
| Endpoint | What does it do? |
| ---------| -----------------|
| POST /v1/visitors/:visitor_id/visits | Add a new Visit record |
| PATCH /v1/visitors/:visitor_id/visits/:id | Update a Visit record |

### Reports
| Endpoint | What does it do? |
| ---------| -----------------|
| GET /v1/retail_chains/summaries | Retail Chain summary |

## License

No License. Daniel Moreto\
You're free to copy, clone or drop everything =)